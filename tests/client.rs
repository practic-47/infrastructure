#[cfg(test)]
mod tests {
    use infrastructure::*;

    #[derive(serde::Serialize)]
    struct TestMessage {
        id: i32,
        test: String,
        test1: String,
        test2: String,
    }

    #[test]
    fn test_publish_message_vec_ok() {
        dotenv::from_filename("local.env").ok();
        let messenger = Messenger::new();
        let subject = "test".to_string();
        let message = vec![0; 8];
        messenger.publish(subject, message);
    }

    #[test]
    fn test_publish_message_struct_ok() {
        dotenv::from_filename("local.env").ok();
        let messenger = Messenger::new();
        let subject = "test".to_string();
        let message = serde_json::to_vec(&TestMessage {
            id: 32,
            test: "Test".to_string(),
            test1: "Test".to_string(),
            test2: "Test".to_string(),
        })
        .expect("Failed to serialize message.");
        messenger.publish(subject, message);
    }

    #[test]
    fn test_subscribe_to_message_ok() {
        dotenv::from_filename("local.env").ok();
        let messenger = Messenger::new();
        let subject = "test".to_string();
        messenger.subscribe(subject, |_msg| {});
    }
}
