use std::sync::Arc;

/// Messenger represents messaging capabilities of the environment.
/// We build an abstractions above real implementation [NATS right now](nats.io).
/// This abstraction provides a set of messaging capabilities to work with.
/// Messenger should be used instead of direct work with an implementation.
#[derive(std::clone::Clone)]
pub struct Messenger {
    nats_client: Arc<natsclient::Client>,
}

impl Messenger {
    pub fn new() -> Self {
        let options = natsclient::ClientOptions::builder()
            .cluster_uris(vec![std::env::var("NATS_URI")
                .expect("NATS_URI environment variable must be set.")
                .into()])
            .connect_timeout(std::time::Duration::from_millis(1000))
            .build()
            .expect("Failed to build NATS Client Options.");
        let client =
            natsclient::Client::from_options(options).expect("Failed to build NATS Client.");
        client.connect().expect("Failed to connect to NATS.");
        Messenger {
            nats_client: Arc::new(client),
        }
    }

    pub fn publish(self: &Self, subject: String, payload: Vec<u8>) {
        match self.nats_client.publish(&subject, &payload, None) {
            Ok(_) => println!("Message sent."),
            Err(err) => println!("Failed to publish NATS message. {}", err),
        }
    }

    pub fn subscribe<F: 'static>(self: &Self, subject: String, function: F)
    where
        F: Fn(Vec<u8>) + Sync + Send,
    {
        self.nats_client
            .subscribe(&subject, move |msg| {
                println!("Received message: {}", msg);
                function(msg.payload.clone());
                Ok(())
            })
            .expect("Failed to subscribe.")
    }
}
