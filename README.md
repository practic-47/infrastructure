# Infrastructure

This library contains common infrastructure entites for Practic's applications.

## Messenger

Abstraction over real cloud messaging system.

- Publish events.
- Send commands.
- Subscribe to events and commands.
